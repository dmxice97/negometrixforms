﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionModelServiceTest
{
    [TestClass]
    public class GetOptionGuidForQuestions_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionsQuestionDTO = new Mock<OptionsQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetOptionGuidForQuestionsAsync(moqOptionsQuestionDTO.Object.Id));
            }

        }
        [TestMethod]
        public async Task Return_Proper_Count_Empty_Of_List_Of_Guids()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Proper_Count_Empty_Of_List_Of_Guids));

            Guid tempGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");
            var mockDateTime = new Mock<IDateTimeProvider>();

            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionsQuestionDTO = new Mock<OptionModelDTO>();
            moqOptionsQuestionDTO.Object.Id = tempGuid;
            var optiont = moqOptionsQuestionDTO.Object.FromDto();
            using (var arrangeContext = new NFContext(options))
            {
                arrangeContext.Options.Add(optiont);
                arrangeContext.SaveChanges();
            }
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);
                var result = await sut.GetOptionGuidForQuestionsAsync(tempGuid);
                List<Guid> listGuids = new List<Guid>();
                var optionQuestionsForForm = await assertContext.OptionQuestions.Where(i => i.FormId == moqOptionsQuestionDTO.Object.Id).ToListAsync();
                foreach (var item in optionQuestionsForForm)
                {
                    listGuids.Add(item.Id);
                }

                Assert.AreEqual(result.Count, listGuids.Count);
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionServiceTest
{
    [TestClass]
    public class CreateOptionQuestion_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionQuestionDTO = new Mock<OptionsQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateAsync(moqOptionQuestionDTO.Object));
            }
        }
        [TestMethod]
        public async Task Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Test_If_Create_Is_Putting_Data_In_DataBase_And_Returns_Correct_Value));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var optionQuestion = new Mock<OptionsQuestionDTO>();

            optionQuestion.Object.Id = testGuid;
            optionQuestion.Object.Name = "Test";

            var toDtoStyle = optionQuestion.Object;
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionQuestionService(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(optionQuestion.Object);

                Assert.AreEqual(resultOQDTO.Id, optionQuestion.Object.Id);
                Assert.AreEqual(resultOQDTO.Name, optionQuestion.Object.Name);
            }
        }
    }
}

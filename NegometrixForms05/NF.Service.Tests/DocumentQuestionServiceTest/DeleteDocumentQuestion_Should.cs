﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.DocumentQuestionService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.DocumentQuestionServiceTest
{
    [TestClass]
    public class DeleteDocumentQuestion_Should
    {
        [TestMethod]
        public async Task Delete_Document_Question()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Delete_Document_Question));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            moqDocumentQuestionDTO.Object.Id = Guid.NewGuid();
            moqDocumentQuestionDTO.Object.IsRequired = true;
            moqDocumentQuestionDTO.Object.Name = "Name";
            moqDocumentQuestionDTO.Object.FileSize = 1;
            moqDocumentQuestionDTO.Object.FileNumbers = 1;

            var documentQuestion = moqDocumentQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.DocumentQuestions.AddAsync(documentQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.DeleteAsync(moqDocumentQuestionDTO.Object.Id);

                Assert.AreEqual(result, moqDocumentQuestionDTO.Object.Name);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);


                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqDocumentQuestionDTO.Object.Id));
            }


        }
        [TestMethod]
        public async Task Throw_When_This_Question_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Question_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqDocumentQuestionDTO = new Mock<DocumentQuestionDTO>();
            moqDocumentQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqDocumentQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new DocumentQuestionService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.DeleteAsync(moqDocumentQuestionDTO.Object.Id));

            }
        }
    }
}

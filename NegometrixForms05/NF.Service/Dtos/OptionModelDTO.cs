﻿using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.Dtos
{
    public class OptionModelDTO
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public bool IsMarked { get; set; } 
        public OptionsQuestion Question { get; set; }
        public Guid OptionsQuestionId { get; set; }
    }
}

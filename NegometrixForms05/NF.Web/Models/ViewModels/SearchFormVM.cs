﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class SearchFormVM
    {
        public string Param { get; set; }
    }
}

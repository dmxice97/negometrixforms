﻿using NF.Models.Models;
using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NF.Service.Dtos
{
    public class FormDTO
    {
        public Guid Id { get; set; }

        [Required]
        [MaxLength(30)]
        [MinLength(3)]
        public string Name { get; set; }
        public ICollection<DocumentQuestionDTO> DocumentQuestions { get; set; }
        public ICollection<OptionsQuestionDTO> OptionsQuestions { get; set; }
        public ICollection<TextQuestionDTO> TextQuestions { get; set; }
        public string Description { get; set; }
        public DateTime ModifiedOn { get; set; } = DateTime.Now.Date;
        public bool IsAnswered { get; set; }
        public DateTime AnsweredOn { get; set; }
        public bool IsAnonymous { get; set; }
        public int AnsweredCount { get; set; }
        public Guid CreatorId { get; set; }
        public Guid AnswerUserId { get; set; }
        public User CreatedBy { get; set; }
    }
}

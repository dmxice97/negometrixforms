﻿using NF.Models.Models;
using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NF.Service.Mappers
{
    public static class OptionsQuestionMapper
    {
        public static OptionsQuestionDTO ToDto(this OptionsQuestion optionsQuestion)
        {
            var optionsQuestionDto = new OptionsQuestionDTO
            {
                Id = optionsQuestion.Id,
                Name = optionsQuestion.Name,
                IsAnswered = optionsQuestion.IsAnswered,
                IsDeleted = optionsQuestion.IsDeleted,
                //new
                FormId = optionsQuestion.FormId,
                IsRequired = optionsQuestion.IsRequired,
                OptionModels = optionsQuestion.OptionModels.ToDto(),
                HasMultipleAnswers=optionsQuestion.HasMultipleAnswers,

            };
            return optionsQuestionDto;
        }

        public static ICollection<OptionsQuestionDTO> ToDto(this ICollection<OptionsQuestion> optionsQuestion)
        {
            var optionsQuestionDto = optionsQuestion.Select(s => s.ToDto()).ToList();
            return optionsQuestionDto;
        }

        public static OptionsQuestion FromDto(this OptionsQuestionDTO optionsQuestionDTO)
        {
            var optionsQuestion = new OptionsQuestion
            {
                Id = optionsQuestionDTO.Id,
                Name = optionsQuestionDTO.Name,
                IsAnswered = optionsQuestionDTO.IsAnswered,
                IsDeleted = optionsQuestionDTO.IsDeleted,
                //new
                FormId = optionsQuestionDTO.FormId,
                IsRequired = optionsQuestionDTO.IsRequired,
                OptionModels = optionsQuestionDTO.OptionModels?.Select(q => q.FromDto()).ToList(),
                HasMultipleAnswers = optionsQuestionDTO.HasMultipleAnswers,
                
            };

            return optionsQuestion;
        }
    }
}

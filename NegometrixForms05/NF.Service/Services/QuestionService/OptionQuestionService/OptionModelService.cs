﻿using Microsoft.EntityFrameworkCore;
using NF.Data;
using NF.Models.Models;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.OptionQuestionService
{
    public class OptionModelService : IOptionModelService
    {
        private readonly NFContext context;
        private readonly IDateTimeProvider dateTimeProvider;
        public OptionModelService(NFContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context;
            this.dateTimeProvider = dateTimeProvider;
        }
        public async Task<bool> CreateAsync(OptionModelDTO model)
        {
            model.Id.IsEmpty();
            var option = model.FromDto();
            await context.Options.AddAsync(option);
            await context.SaveChangesAsync();
            return true;
        }
        public async Task<OptionModelDTO> UpdateAsync(OptionModelDTO model)
        {
            model.Id.IsEmpty();
            var toBeUpdated = await this.context.Options
             .FirstOrDefaultAsync(x => x.Id == model.Id);

            toBeUpdated.CheckForNull(Validator.nullMsg);
            toBeUpdated.IsMarked = model.IsMarked;
            toBeUpdated.Text = model.Text;

            this.context.Update(toBeUpdated);
            await this.context.SaveChangesAsync();
            return model;
        }
        public async Task<List<Guid>> GetOptionGuidForQuestionsAsync(Guid id)
        {
            id.IsEmpty();
            List<Guid> listGuids = new List<Guid>();
            var optionForQuestion = await this.context.Options.Where(i => i.OptionsQuestionId == id).ToListAsync();
            foreach (var item in optionForQuestion)
            {
                listGuids.Add(item.Id);
            }
            return listGuids;
        }
        public async Task<string> DeleteAsync(Guid id)
        {
            id.IsEmpty();

            var optionQuestion = await this.context.Options
                .FirstOrDefaultAsync(q => q.Id == id);

            optionQuestion.CheckForNull(Validator.nullMsg);
            this.context.Remove(optionQuestion);
            await this.context.SaveChangesAsync();
            return optionQuestion.Text;
        }
    }
}

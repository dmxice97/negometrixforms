﻿using NF.Service.Dtos;
using NF.Service.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.DocumentQuestionService
{
    public interface IDocumentQuestionService
    {
        Task<DocumentQuestionDTO> CreateAsync(DocumentQuestionDTO documentQuestionDTO);
        Task<DocumentQuestionDTO> UpdateAsync(DocumentQuestionDTO model);
        Task<string> DeleteAsync(Guid id);
        Task<List<Guid>> GetDocumentQuestionsGuidsAsync(Guid id);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.TextQuestionService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Service.Tests.TextQuestionServiceTest
{
    [TestClass]
    public class GetTextQuestionsGuid_Should
    {
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqTextQuestionDTO = new Mock<FormDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetTextQuestionsGuidsAsync(moqTextQuestionDTO.Object.Id));
            }
        }
        [TestMethod]
        public async Task Return_Proper_Count_Empty_Of_List_Of_Guids()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Return_Proper_Count_Empty_Of_List_Of_Guids));

            Guid tempGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");
            var mockDateTime = new Mock<IDateTimeProvider>();

            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var textQuestionDTO = new Mock<TextQuestionDTO>();
            textQuestionDTO.Object.Id = tempGuid;
            var optiont = textQuestionDTO.Object.FromDto();
            using (var arrangeContext = new NFContext(options))
            {
                arrangeContext.TextQuestions.Add(optiont);
                arrangeContext.SaveChanges();
            }
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object);
                var result = await sut.GetTextQuestionsGuidsAsync(tempGuid);
                List<Guid> listGuids = new List<Guid>();
                var optionQuestionsForForm = await assertContext.OptionQuestions.Where(i => i.FormId == textQuestionDTO.Object.Id).ToListAsync();
                foreach (var item in optionQuestionsForForm)
                {
                    listGuids.Add(item.Id);
                }

                Assert.AreEqual(result.Count, listGuids.Count);
            }
        }
    }
}

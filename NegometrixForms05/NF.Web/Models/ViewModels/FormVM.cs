﻿using NF.Models.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class FormVM
    {
        public FormVM()
        {
            this.DocumentQuestions = new List<DocumentQuestionVM>();
            this.OptionsQuestions = new List<OptionsQuestionVM>();
            this.TextQuestions = new List<TextQuestionVM>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CreatorUsername { get; set; }
        public ICollection<DocumentQuestionVM> DocumentQuestions { get; set; }
        public ICollection<OptionsQuestionVM> OptionsQuestions { get; set; }
        public ICollection<TextQuestionVM> TextQuestions { get; set; }
        public List<DocumentQuestionVM> DocumentQuestionVMs { get; set; }
        public List<OptionsQuestionVM> OptionsQuestionVMs { get; set; }
        public List<TextQuestionVM> TextQuestionVMs { get; set; }
        [Required]

        public string Description { get; set; }
        public bool IsAnswered { get; set; }
        public bool IsAnonymous { get; set; }
        public int AnsweredCount { get; set; }
        public Guid AnswerUserId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid CreatorId { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NF.Models.Models.FormModel.QuestionModels
{
    public class FormAnswers
    {
        [Key]
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public Guid AnsweredBy { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime AnswerDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string AnseredUserName { get; set; }
        public string FormName { get; set; }


        //Content
        public string OptionQuestionAnswers { get; set; }
        public string DocumentQuestionAnswers { get; set; }
        public string TextQuestionAnswers { get; set; }

       
    }
}

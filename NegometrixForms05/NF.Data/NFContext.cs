﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NF.Data.Config;
using NF.Models.Models;
using NF.Models.Models.FormModel.QuestionModels;
using NF.Models.Models.Role;
using System;
using System.Collections.Generic;

namespace NF.Data
{
    public class NFContext : IdentityDbContext<User, Role, Guid>
    {
        public NFContext(DbContextOptions<NFContext> options)
            : base(options)
        {

        }
        public NFContext()
        {

        }
       
        //Form
        public DbSet<Form> Forms { get; set; }
        //Answers
        public DbSet<FormAnswers> FormAnswers { get; set; }

        //Questions
        public DbSet<TextQuestion> TextQuestions { get; set; }
        public DbSet<OptionsQuestion> OptionQuestions { get; set; }
        public DbSet<OptionModel> Options { get; set; }
        public DbSet<DocumentQuestion> DocumentQuestions { get; set; }
        public DbSet<DocumentPath> DocumentPaths { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new FormConfig());
            modelBuilder.ApplyConfiguration(new FormAnswersConfig());
            modelBuilder.ApplyConfiguration(new DocumentQuestionConfig());
            modelBuilder.ApplyConfiguration(new OptionsQuestionConfig());
            modelBuilder.ApplyConfiguration(new TextQuestionConfig());
            modelBuilder.ApplyConfiguration(new OptionModelConfig());
            modelBuilder.ApplyConfiguration(new DocumentPathConfig());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}

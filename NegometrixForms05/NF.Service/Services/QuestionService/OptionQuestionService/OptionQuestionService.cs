﻿using Microsoft.EntityFrameworkCore;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.DTOs;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Services.QuestionService.OptionQuestionService
{
    public class OptionQuestionService : IOptionQuestionService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly NFContext context;

        public OptionQuestionService(NFContext context, IDateTimeProvider dateTimeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(context));

        }
        public async Task<List<Guid>> GetOptionQuestionsGuidsAsync(Guid id)
        {
            id.IsEmpty(Validator.nullMsg);

            List<Guid> listGuids = new List<Guid>();
            var optionQuestionsForForm = await this.context.OptionQuestions.Where(i => i.FormId == id).ToListAsync();

            //optionQuestionsForForm.CheckForListCount(Validator.nullMsg);
            foreach (var item in optionQuestionsForForm)
            {
                listGuids.Add(item.Id);
            }
            return listGuids;
        }

        public async Task<OptionsQuestionDTO> CreateAsync(OptionsQuestionDTO optionQuestionDTO)
        {
            optionQuestionDTO.Id.IsEmpty(Validator.nullMsg);

            var oq = optionQuestionDTO.FromDto();
            oq.CreatedOn = this.dateTimeProvider.GetDateTime();

            await this.context.OptionQuestions.AddAsync(oq);
            await this.context.SaveChangesAsync();
            return optionQuestionDTO;
        }
        
        public async Task<string> DeleteAsync(Guid id)
        {
            id.IsEmpty(Validator.nullMsg);

            var optionQuestion = await this.context.OptionQuestions
                .Where(q => q.IsDeleted == false)
                .FirstOrDefaultAsync(q => q.Id == id);

            optionQuestion.CheckForNull(Validator.nullMsg);
            optionQuestion.IsDeleted = true;
            optionQuestion.DeletedOn = this.dateTimeProvider.GetDateTime();
            this.context.Remove(optionQuestion);
            await this.context.SaveChangesAsync();
            return optionQuestion.Name;
        }
        public async Task<OptionsQuestionDTO> UpdateAsync(OptionsQuestionDTO model)
        {
            model.Id.IsEmpty();
            var toBeUpdated = this.context.OptionQuestions
                .FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);

            toBeUpdated.CheckForNull(Validator.nullMsg);

            toBeUpdated.ModifiedOn = this.dateTimeProvider.GetDateTime();
            toBeUpdated.Name = model.Name;
            toBeUpdated.IsRequired = model.IsRequired;
            toBeUpdated.IsAnswered = model.IsAnswered;
            toBeUpdated.HasMultipleAnswers = model.HasMultipleAnswers;
            
            this.context.Update(toBeUpdated);
            await this.context.SaveChangesAsync();
            return model;
        }
    }
}

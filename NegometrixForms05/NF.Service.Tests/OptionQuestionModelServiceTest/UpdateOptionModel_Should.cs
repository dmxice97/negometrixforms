﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Mappers;
using NF.Service.Services.QuestionService.OptionQuestionService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.OptionQuestionModelServiceTest
{
    [TestClass]
    public class UpdateOptionModel_Should
    {

        [TestMethod]
        public async Task Update_Option_For_OptionQuestion()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Update_Option_For_OptionQuestion));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionForQuestionDTO = new Mock<OptionModelDTO>();
            moqOptionForQuestionDTO.Object.Id = Guid.NewGuid();
            moqOptionForQuestionDTO.Object.Text = "Text;";
            moqOptionForQuestionDTO.Object.IsMarked = true;

            var optionModelForQuestion = moqOptionForQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);
                //add to mock database
                await assertContext.Options.AddAsync(optionModelForQuestion);
                await assertContext.SaveChangesAsync();

                var result = await sut.UpdateAsync(moqOptionForQuestionDTO.Object);
                var actual = assertContext.Options.FindAsync(moqOptionForQuestionDTO.Object.Id).Result;

                Assert.AreEqual(result.Id, actual.Id);
                Assert.AreEqual(result.Text, actual.Text);
                Assert.AreEqual(result.IsMarked, actual.IsMarked);
            }
        }
        [TestMethod]
        public async Task Throw_When_Id_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_Id_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //set null value for name
            var moqOptionForQuestionDTO = new Mock<OptionModelDTO>();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);


                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqOptionForQuestionDTO.Object));
            }


        }
        [TestMethod]
        public async Task Throw_When_This_Option_IsNotIn_Database()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_This_Option_IsNotIn_Database));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            var moqOptionForQuestionDTO = new Mock<OptionModelDTO>();
            moqOptionForQuestionDTO.Object.Id = Guid.NewGuid();

            var textQuestion = moqOptionForQuestionDTO.Object.FromDto();
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new OptionModelService(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.UpdateAsync(moqOptionForQuestionDTO.Object));

            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.Dtos;
using NF.Service.General.Contracts;
using NF.Service.Services.FormService;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace NF.Service.Tests.FormAnswerTest
{
    [TestClass]
    public class CreateFormAnswer_Should
    {
        [TestMethod]
        public async Task Create_Answer()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Create_Answer));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);

            Guid testGuid = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B029D");

            var moqFormDTO = new Mock<FormDTO>();

            moqFormDTO.Object.Id = testGuid;
            moqFormDTO.Object.Name = "Test";

            var formanswerDTO = new FormAnswerDTO();
            formanswerDTO.FormId = testGuid;
            formanswerDTO.FormName = "Test";

            var toDtoStyle = moqFormDTO.Object;

            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);
                var resultOQDTO = await sut.CreateAsync(moqFormDTO.Object);
                await sut.CreateAnswerFormAsync(formanswerDTO);

                Assert.AreEqual(formanswerDTO.FormName, moqFormDTO.Object.Name);
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Models.ViewModels
{
    public class DocumentPathVM
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public Guid DocumentQuestionId { get; set; }
        public DocumentQuestionVM DocumentQuestionVM { get; set; }

    }
}

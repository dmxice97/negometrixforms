﻿using NF.Models.Models;
using NF.Service.Dtos;
using NF.Service.Mappers;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class QuestionFormsMapper
    {
    
        public static FormDTO ToDto(this FormVM questionFormVM)
        {
            var questionFormDTO = new FormDTO
            {
                Id=questionFormVM.Id,
                Name=questionFormVM.Name,
                AnsweredCount=questionFormVM.AnsweredCount,
                IsAnonymous=questionFormVM.IsAnonymous,
                DocumentQuestions=questionFormVM.DocumentQuestions?.Select(q => q.ToDto()).ToList(),
                OptionsQuestions=questionFormVM.OptionsQuestions?.Select(q => q.ToDto()).ToList(),
                TextQuestions = questionFormVM.TextQuestions?.Select(q => q.ToDto()).ToList(),
                AnswerUserId =questionFormVM.AnswerUserId,
                IsAnswered=questionFormVM.IsAnswered,
                ModifiedOn=questionFormVM.ModifiedOn.Date,
                CreatorId=questionFormVM.CreatorId,
                Description= questionFormVM.Description,
            };
            return questionFormDTO;
        }
        public static ICollection<FormDTO> ToDto(this ICollection<FormVM> questionFormVM)
        {
            var questionFormDTO = questionFormVM.Select(s => s.ToDto()).ToList();

            return questionFormDTO;
        }
        public static ICollection<FormVM> FromDto(this ICollection<FormDTO> questionFormDTO)
        {
            var questionFormVM = questionFormDTO.Select(s => s.FromDto()).ToList();

            return questionFormVM;
        }
        public static FormVM FromDto(this FormDTO questionFormDTO)
        {
            var date = questionFormDTO.ModifiedOn.Date;
            var questionFormVM = new FormVM
            {
                Id = questionFormDTO.Id,
                Name = questionFormDTO.Name,
                AnsweredCount = questionFormDTO.AnsweredCount,
                IsAnonymous = questionFormDTO.IsAnonymous,
                DocumentQuestions = questionFormDTO.DocumentQuestions?.Select(q => q.FromDto()).ToList(),
                OptionsQuestions = questionFormDTO.OptionsQuestions?.Select(q => q.FromDto()).ToList(),
                TextQuestions = questionFormDTO.TextQuestions?.Select(q => q.FromDto()).ToList(),
                AnswerUserId = questionFormDTO.AnswerUserId,
                IsAnswered = questionFormDTO.IsAnswered,
                ModifiedOn= date,
                CreatorId=questionFormDTO.CreatorId,
                Description =questionFormDTO.Description,
                DocumentQuestionVMs= questionFormDTO.DocumentQuestions?.Select(q => q.FromDto()).ToList(),
                OptionsQuestionVMs=questionFormDTO.OptionsQuestions?.Select(q => q.FromDto()).ToList(),
                TextQuestionVMs = questionFormDTO.TextQuestions?.Select(q => q.FromDto()).ToList(),
            };
            return questionFormVM;
        }
    }
}

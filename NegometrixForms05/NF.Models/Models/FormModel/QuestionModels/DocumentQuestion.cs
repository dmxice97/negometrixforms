﻿using Microsoft.AspNetCore.Http;
using NF.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NF.Models.Models
{

    public class DocumentQuestion : IEntityQuestions
    {
        [Key]
        public Guid Id { get; set; }
        public int FileNumbers { get; set; }
        public int FileSize { get; set; }

        //Answers as document paths
        public ICollection<DocumentPath> Paths { get; set; }

        //new
        public string DocumentPath { get; set; }

        //EntityQuestions
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsAnswered { get; set; } = false;
        public bool IsRequired { get; set; } = false;

        //DateTime
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }

    }
}

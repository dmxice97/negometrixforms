﻿using Newtonsoft.Json;
using NF.Service.Dtos;
using NF.Web.Models.AnswerViewModels;
using NF.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NF.Web.Mappers
{
    public static class FormAnswerMapperVM
    {
        public static FormAnswerDTO ToDto(this FormAnswerAVM questionFormVM)
        {
            var textQ = JsonConvert.SerializeObject(questionFormVM.TextQuestionAnswers);
            var docQ = JsonConvert.SerializeObject(questionFormVM.DocumentQuestionAnswers);
            var optionQ = JsonConvert.SerializeObject(questionFormVM.OptionQuestionAnswers);

            var questionFormDTO = new FormAnswerDTO
            {

                Id = questionFormVM.Id,
                FormId = questionFormVM.FormId,
                AnsweredBy = questionFormVM.AnsweredBy,
                AnswerDate = questionFormVM.AnswerDate,
                IsAnonymous = questionFormVM.IsAnonymous,
                IsDeleted = questionFormVM.IsDeleted,
                DeletedOn = questionFormVM.DeletedOn,
                
                AnseredUserName = questionFormVM.AnseredUserName,
                FormName = questionFormVM.FormName,
                OptionQuestionAnswers = optionQ,
                TextQuestionAnswers = textQ,
                DocumentQuestionAnswers = docQ
            };
            return questionFormDTO;
        }
        public static ICollection<FormAnswerDTO> ToDto(this ICollection<FormAnswerAVM> questionFormVM)
        {
            var questionFormDTO = questionFormVM.Select(s => s.ToDto()).ToList();

            return questionFormDTO;
        }
        public static ICollection<FormAnswerAVM> FromDto(this ICollection<FormAnswerDTO> questionFormDTO)
        {
            var questionFormVM = questionFormDTO.Select(s => s.FromDto()).ToList();

            return questionFormVM;
        }
        public static FormAnswerAVM FromDto(this FormAnswerDTO questionFormDTO)
        {
            var textQ = JsonConvert.DeserializeObject<List<TextQuestionAVM>>(questionFormDTO.TextQuestionAnswers);
            var docQ = JsonConvert.DeserializeObject<List<DocumentQuestionAVM>>(questionFormDTO.DocumentQuestionAnswers);
            var optionQ = JsonConvert.DeserializeObject<List<OptionQuestionAVM>>(questionFormDTO.OptionQuestionAnswers);

            var questionFormVM = new FormAnswerAVM
            {
                Id = questionFormDTO.Id,
                FormId = questionFormDTO.FormId,
                AnsweredBy = questionFormDTO.AnsweredBy,
                AnswerDate = questionFormDTO.AnswerDate.Date,
                IsAnonymous = questionFormDTO.IsAnonymous,
                IsDeleted = questionFormDTO.IsDeleted,
                DeletedOn = questionFormDTO.DeletedOn,
                AnseredUserName = questionFormDTO.AnseredUserName,
                FormName = questionFormDTO.FormName,
                OptionQuestionAnswers = optionQ,
                TextQuestionAnswers = textQ,
                DocumentQuestionAnswers = docQ
            };
            return questionFormVM;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NF.Models.Models
{
    public class OptionModel
    {
        [Key]
        public Guid Id { get; set; }
        public string Text { get; set; }
        public bool IsMarked { get; set; } = false;
        public OptionsQuestion Question { get; set; }
        public Guid OptionsQuestionId { get; set; }
    }
}

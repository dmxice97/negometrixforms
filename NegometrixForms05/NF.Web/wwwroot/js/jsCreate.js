  question_count = 0;
    option_count = {};

   
    //$('.go_up').click(function () {
    //    $("html, body").animate({ scrollTop: 0 }, 600);
    //    return false;
    //});

    function get_state() {

        var questions = []
        for (var i = 1; i <= question_count; i++) {
            if ($('#question_' + i).length > 0) {

                var text = $('#question_' + i + '_text').val();
                var required_answer = $('#question_' + i + '_required_answer_checkbox').is(':checked');
                var question_type = $('#question_' + i).attr("data-type");

                if (question_type == "options") {

                    var multiple_answers = $('#question_' + i + '_multiple_answers_checkbox').is(':checked');
                    var options = []
                    for (var j = 1; j <= option_count[i]; j++) {
                        if ($('#question_' + i + '_option_' + j + '_text').val()) {
                            options.push($('#question_' + i + '_option_' + j + '_text').val());
                        }
                    }
                    questions.push({ "type": "options", "text": text, "multiple_answers": multiple_answers, "required_answer": required_answer, "options": options });

                } else if (question_type == "text") {

                    var long_answer = $('#question_' + i + '_long_answer_checkbox').is(':checked');

                    questions.push({ "type": "text", "text": text, "long_answer": long_answer, "required_answer": required_answer });

                } else if (question_type == "document") {

                    var file_number_limit = $('#question_' + i + '_file_number_limit').val();
                    var file_size_limit = $('#question_' + i + '_file_size_limit').val();

                    questions.push({ "type": "document", "text": text, "file_number_limit": file_number_limit, "file_size_limit": file_size_limit, "required_answer": required_answer });

                }
            }
        }

        var state = { "name": $("#form_name").val(), "description": $("#form_description").val(), "questions": questions }

        console.log(state);

        return state;

    }

    $(document).ready(function () {

        $(document).on('click', '#add_question_button', function () {
            if ($('#question_type_select').val() == "Options") {

                question_count++;

                new_question = $("#option_question_template").clone(true);
                new_question.css('display', 'block');
                new_question.prop('id', 'question_' + question_count);
                new_question.find('.remove_question_button').attr('data-parent', 'question_' + question_count);
                new_question.find('.question_text').prop('id', 'question_' + question_count + '_text');
                new_question.find("#multiple_answers_checkbox").prop('id', 'question_' + question_count + '_multiple_answers_checkbox');
                new_question.find("#multiple_answers_label").prop('for', 'question_' + question_count + '_multiple_answers_checkbox');
                new_question.find("#multiple_answers_label").prop('id', '');
                new_question.find("#required_answer_checkbox").prop('id', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('for', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('id', '');
                new_question.find(".option_1").prop('id', 'question_' + question_count + '_option_1_text');
                new_question.find(".option_2").prop('id', 'question_' + question_count + '_option_2_text');
                new_question.find(".additional_options").prop('id', 'question_' + question_count + '_additional_options');
                new_question.find(".add_option_button").val(question_count);

                // 2 options by default
                option_count[question_count] = 2;

                $("#questions_div").append(new_question);

            } else if ($('#question_type_select').val() == "Text") {

                question_count++;
                new_question = $("#text_question_template").clone(true);
                new_question.css('display', 'block');
                new_question.prop('id', 'question_' + question_count);
                new_question.find('.remove_question_button').attr('data-parent', 'question_' + question_count);
                new_question.find('.question_text').prop('id', 'question_' + question_count + '_text');
                new_question.find("#long_answer_checkbox").prop('id', 'question_' + question_count + '_long_answer_checkbox');
                new_question.find("#long_answer_label").prop('for', 'question_' + question_count + '_long_answer_checkbox');
                new_question.find("#long_answer_label").prop('id', '');
                new_question.find("#required_answer_checkbox").prop('id', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('for', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('id', '');

                $("#questions_div").append(new_question);

            } else if ($('#question_type_select').val() == "File") {

                question_count++;
                new_question = $("#document_question_template").clone(true);
                new_question.css('display', 'block');
                new_question.prop('id', 'question_' + question_count);
                new_question.find('.remove_question_button').attr('data-parent', 'question_' + question_count);
                new_question.find('.question_text').prop('id', 'question_' + question_count + '_text');
                new_question.find('#file_number_limit').prop('id', 'question_' + question_count + '_file_number_limit');
                new_question.find('#file_size_limit').prop('id', 'question_' + question_count + '_file_size_limit');
                new_question.find("#required_answer_checkbox").prop('id', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('for', 'question_' + question_count + '_required_answer_checkbox');
                new_question.find("#required_answer_label").prop('id', '');

                $("#questions_div").append(new_question);

            }
        });

        $(document).on('click', '.add_option_button', function () {
            var this_number = parseInt($(this).val());

            option_count[this_number] = option_count[this_number] + 1;

            new_option = $("#option_template").clone(true);
            new_option.css('display', 'block');
            new_option.prop('id', 'question_' + $(this).val() + '_option_' + option_count[this_number]);
            new_option.find('.remove_option_button').attr('data-parent', 'question_' + $(this).val() + '_option_' + option_count[this_number]);
            new_option.find('.option_text_input').prop('id', 'question_' + $(this).val() + '_option_' + option_count[this_number] + '_text');

            $("#question_" + $(this).val() + "_additional_options").append(new_option);
        });

        $(document).on('click', '.remove_option_button', function () {
            var parent_id = $(this).attr('data-parent');
            $('#' + parent_id).remove();
        });

        $(document).on('click', '.remove_question_button', function () {
            var parent_id = $(this).attr('data-parent');
            $('#' + parent_id).remove();
        });

        $(document).on('submit', '#new_form', function () {

            $("#state_input").val(JSON.stringify(get_state()));

            return true;

        });

    });
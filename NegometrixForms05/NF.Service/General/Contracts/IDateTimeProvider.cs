﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NF.Service.General.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}

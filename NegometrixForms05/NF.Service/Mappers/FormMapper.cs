﻿using NF.Models.Models;
using NF.Service.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace NF.Service.Mappers
{
    public static class FormMapper
    {

        public static FormDTO ToDto(this Form questionForm)
        {
            var questionFormDTO = new FormDTO
            {
                Id = questionForm.Id,
                Name = questionForm.Name,

                TextQuestions = questionForm.TextQuestions?.Select(q => q.ToDto()).ToList(),
                DocumentQuestions = questionForm.DocumentQuestions?.Select(q => q.ToDto()).ToList(),
                OptionsQuestions = questionForm.OptionsQuestions?.Select(q => q.ToDto()).ToList(),
                AnsweredCount = questionForm.AnswersCount,
                CreatedBy = questionForm.CreatedBy,
                CreatorId = questionForm.CreatorId,
                //IsDeleted = questionForm.IsDeleted,
                Description = questionForm.Description,
                ModifiedOn = questionForm.ModifiedOn,

            };
            return questionFormDTO;
        }
        public static ICollection<FormDTO> ToDto(this ICollection<Form> questionForm)
        {
            var questionFormDTO = questionForm.Select(q => q.ToDto()).ToList();
            return questionFormDTO;
        }
        public static Form FromDto(this FormDTO questionFormDTO)
        {
            try
            {
            var questionForm = new Form
            {
                Id = questionFormDTO.Id,
                Name = questionFormDTO.Name,
                //TextQuestions = questionFormDTO.TextQuestions.GetEntities(),
                TextQuestions = questionFormDTO.TextQuestions?.Select(q => q.FromDto()).ToList(),
                DocumentQuestions = questionFormDTO.DocumentQuestions?.Select(q => q.FromDto()).ToList(),
                OptionsQuestions = questionFormDTO.OptionsQuestions?.Select(q => q.FromDto()).ToList(),
                CreatedBy = questionFormDTO.CreatedBy,
                CreatorId = questionFormDTO.CreatorId,
                AnswersCount = questionFormDTO.AnsweredCount,
                //IsDeleted=questionFormDTO.IsDeleted,
                Description = questionFormDTO.Description,
                ModifiedOn = questionFormDTO.ModifiedOn,
            };
                return questionForm;
            }
            catch (System.Exception)
            {

                throw;
            }

        }
    }
}

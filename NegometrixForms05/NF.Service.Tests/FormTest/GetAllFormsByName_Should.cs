﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NF.Data;
using NF.Service.General.Contracts;
using NF.Service.Services.FormService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NF.Service.Tests.FormTest
{
    [TestClass]
    public class GetAllFormsByName_Should
    {
        [TestMethod]
        public async Task Throw_When_String_Empty()
        {
            //Arrange

            var options = Utils.GetOptions(nameof(Throw_When_String_Empty));
            var mockDateTime = new Mock<IDateTimeProvider>();
            mockDateTime.Setup(x => x.GetDateTime()).Returns(DateTime.UtcNow);
            //Act & Assert
            using (var assertContext = new NFContext(options))
            {
                var sut = new FormServices(assertContext, mockDateTime.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAllByNameAsync(String.Empty));
            }
        }
    }
}
